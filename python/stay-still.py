#!/usr/bin/python3

import sys
import select
import cv2
from argparse import ArgumentParser
from threading import Thread
from http.server import HTTPServer, BaseHTTPRequestHandler

from utils import InputCamera, OutputVideo, GUIWindow


class HTTPServerThread(Thread):
    def __init__(self, app):
        super().__init__()
        self.app = app
        self.daemon = True
        self.start()

    def run(self):
        this = self

        class RequestHandler(BaseHTTPRequestHandler):
            def do_GET(self):
                print("- received GET from http...")
                this.app.toggle_mode()

        server = HTTPServer(('', 8000), RequestHandler)
        server.serve_forever()


class StayStillCam:
    def __init__(self, args):
        self.args = args
        self.running = True
        self.size = list(map(int, args.size.split(",")))
        self.still_frame = None
        self.still_mode = False

        self.cam = InputCamera(self.args.in_device, self.size)
        self.out = OutputVideo(self.args.out_device, self.size)
        self.http_server = HTTPServerThread(self)
        self.create_user_iface()

    def create_user_iface(self):
        if not self.args.nogui:
            self.gui = GUIWindow("Stay Still Camera toy")
            self.gui.set_input_handler(self.on_key_event)

    def update_ui(self, frame):
        if not self.args.nogui:
            self.gui.show(frame)
            self.gui.check_kb()
        else:
            i, _, _ = select.select([sys.stdin], [], [], 0.01)
            if i:
                code = ord(sys.stdin.read(1))
                sys.stdin.flush()
                self.on_key_event(code)

    def run(self):
        print("\n\n- 's' to toggle between stil/live modes")
        print("- 'c' to acquire still frame")
        print("- 'q' to quit application")

        self.get_still_frame()
        while self.running:
            if not self.still_mode:
                frame = self.cam.get()
            else:
                frame = self.still_frame

            self.out.write(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
            self.update_ui(frame)

    def on_key_event(self, key):
        if key in [27, 113]:  # esc, 'q'
            self.running = False
        elif key == 99:       # 'c'
            self.get_still_frame()
        elif key == 115:      # 's'
            self.toggle_mode()

    def get_still_frame(self):
        self.still_frame = self.cam.get()
        print("- still frame updated")

    def toggle_mode(self):
        self.still_mode = not self.still_mode
        print("- switch to " + ("still" if self.still_mode else "live") + " mode")


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-i", "--in-device", default="/dev/video0",
        help="input video device to use")
    parser.add_argument("-o", "--out-device", default="/dev/video6",
        help="output video device to use")
    parser.add_argument("-s", "--size", default="640,480",
        help="size (width,height) of captured frames")
    parser.add_argument("--no-gui", dest="nogui", action="store_true",
        help="disable the graphical user interface")

    StayStillCam(parser.parse_args()).run()