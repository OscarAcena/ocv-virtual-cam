#!/usr/bin/python3

from http.server import HTTPServer, SimpleHTTPRequestHandler
import ssl


httpd = HTTPServer(('localhost', 4443), SimpleHTTPRequestHandler)
httpd.socket = ssl.wrap_socket(
    httpd.socket,
    keyfile="static/ssl/key.pem",
    certfile='static/ssl/cert.pem',
    server_side=True)

print("Server ready, open: https://localhost:4443/sink.html")
print("NOTE: You would need to allow the auto-generated cert!")
httpd.serve_forever()